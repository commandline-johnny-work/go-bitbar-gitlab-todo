# go-bitbar-gitlab-todo

BitBar Plugin to display GitLab Todos written in golang.
Inspired by https://gitlab.com/dimitrieh/dotfiles/tree/master/bitbar

## Download Binaries

You can download the latest release, from [here](https://gitlab.com/therealryanbonham/go-bitbar-gitlab-todo/-/releases)

## Installation From Source

Go 1.15 is used to compile this plugin.

```bash
go build -o gitlab.5m.cgo ./...
```

- Move the gitlab.5m.cgo file to your bitbar plugin folder.
- Create a config folder in your bitbar plugin directory and create a [config.yaml](https://gitlab.com/therealryanbonham/go-bitbar-gitlab-todo/-/blob/main/config/config.sample.yaml) file.
- Edit config.yaml and add your gitlab token.

## Features

- Supports Gitlab.com and self hosted gitlab instances.
- Show Todos for Issues and Merge Request
- Customization of FontSize, FontFamily, and Header Color of the bitbar text.
- Optionally Show Gitlab Pipeline status for MR Todos
- Optionally show Priority Icon "❗️" in the menubar
- Show optional user defined set of labels for MR and Issue Todos
- Filter MR and Issue Todos by keywords
- Prioritize MR Todos by Keywords
- Prioritize MR Todos based on number of approvers.
- Prioritize MR Todos by Gitlab Group
- Optionally Hide Todos based on Repo Path, uses regex.
- Optionally Auto Close Todos where associated MR is closed/merged.
- Ability to Filter Todos based on MR / Issue Labels
- Ability to Hide Todos for MRs with pipelines still running.

## Contributing

[See the contributing guide](https://gitlab.com/therealryanbonham/go-bitbar-gitlab-todo/-/blob/main/CONTRIBUTING.md)

## License

[MIT](https://choosealicense.com/licenses/mit/)
